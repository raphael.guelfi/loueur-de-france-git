
// init controller
var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: 900}});

// build scenes
new ScrollMagic.Scene({triggerElement: "#sec1"})
                .setClassToggle("#high1", "active",) // add class toggle
                
                .addTo(controller);
                
new ScrollMagic.Scene({triggerElement: "#sec2"})
                .setClassToggle("#high2", "active") // add class toggle
                
                .addTo(controller);
new ScrollMagic.Scene({triggerElement: "#sec3"})
                .setClassToggle("#high3", "active") // add class toggle
                
                .addTo(controller);
new ScrollMagic.Scene({triggerElement: "#sec4"})
                .setClassToggle("#high4", "active") // add class toggle
                
                .addTo(controller);
new ScrollMagic.Scene({triggerElement: "#sec5"})
                .setClassToggle("#high5", "active") // add class toggle
                
                .addTo(controller);
new ScrollMagic.Scene({triggerElement: "#sec6"})
                .setClassToggle("#high6", "active") // add class toggle
                
                .addTo(controller);

