$(document).ready(function(){

      var owl = $('.owl-carousel');
      owl.owlCarousel({
          loop:true,
          nav:true,
          margin:10,
          center:true,
          responsive:{
              0:{
                  items:2
              },
              600:{
                  items:3
              },            
              960:{
                  items:3
              },
              1200:{
                  items:3
              }
          }
      });
     
      
  });